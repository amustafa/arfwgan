import time
import torch
import os
import gc
from gen_models_ar import *
import numpy as np
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.utils.data import DataLoader
from torch import optim
from data_generator_lpcnet import AudioSampleGenerator
import random
from stft_loss import *

def fetch_data(batch, batch_size, device):
    
    wb_speech = batch['audio'].type(torch.FloatTensor).view(batch_size,8000).to(device)
    pw_speech = batch['pw_audio'].type(torch.FloatTensor).view(batch_size,8000).to(device)
    periods = batch['periods'].type(torch.long).view(batch_size,50).to(device)
    bfcc_with_corr = batch['bfcc_with_corr'].type(torch.FloatTensor).to(device)

    return wb_speech, pw_speech, periods, bfcc_with_corr

def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True

def _remove_weight_norm(m):
    try:
        torch.nn.utils.remove_weight_norm(m)
    except ValueError:  # this module didn't have weight norm
        return

if __name__ == "__main__":

    torch.cuda.init()
    seed_everything(5)
    out_path = 'trained_models' 

    #change the next two lines accoring to ur dataset files
    audio_data_file = 'datasets/data_gan2.s16' 
    feat_data_file_actual = 'features_gan2.f32' 

    model_fdr = 'models'  
    opt_fdr='optimizers'
    # time info is used to distinguish dfferent training sessions

    run_time = time.strftime('%Y%m%d_%H%M', time.gmtime())  

    # create folder for model checkpoints
    models_path = os.path.join(os.getcwd(), out_path, run_time, model_fdr)
    if not os.path.exists(models_path):
        os.makedirs(models_path)

    # create folder for optimizer checkpoints
    optimizer_path = os.path.join(os.getcwd(), out_path, run_time, opt_fdr)
    if not os.path.exists(optimizer_path):
        os.makedirs(optimizer_path)

    batch_size = 1024

    sample_generator = AudioSampleGenerator(audio_data_file, feat_data_file_actual, batch_size)
    random_data_loader = DataLoader(
                dataset=sample_generator,
                batch_size=batch_size, 
                num_workers=20,
                shuffle=False,
                pin_memory= True,
                drop_last=True
                )    
             
    print('DataLoader created\n')
    
    g_learning_rate = 0.0005

    #please change this next line based on ur cuda device id
    device = torch.device('cuda:5')

    generator = PSARFWGAN()
    generator.to(device)
    g_optimizer = optim.AdamW(generator.parameters(), g_learning_rate, betas=[0.8, 0.99])
    
    '''saved_model_path= 'trained_models_distill/20230903_0136/models/generator.pkl'
    saved_model_generator= torch.load(saved_model_path)
    generator.load_state_dict(saved_model_generator)

    saved_opt_path= 'trained_models_distill/20230903_0136/optimizers/g_opt.pkl'
    saved_model_opt= torch.load(saved_opt_path)
    g_optimizer.load_state_dict(saved_model_opt)
    for param_group in g_optimizer.param_groups:
        param_group['lr'] = g_learning_rate'''

    spect_loss =  MultiResolutionSTFTLoss(device).to(device)
   
    print('All models have been created\n')

    starting_epoch=0
    print('Start Training...')

    # I am setting a crazy number of training epochs as I usually stop training explicitly 
    for epoch in range(starting_epoch,100000):   
        for i, sample_batch in enumerate(random_data_loader):

            wb_speech, pw_speech, periods, bfcc_with_corr = fetch_data(sample_batch, batch_size, device)
            x0 = wb_speech[:,:320]
            
            ###################### Train G ######################
            generator.zero_grad()
            g_optimizer.zero_grad()

            speech_generated = generator(periods.detach(), bfcc_with_corr.detach(), x0.detach()) 

            tf_loss = spect_loss(speech_generated, wb_speech.detach())

            cos_dist_loss =  torch.mean(1.0 - F.cosine_similarity(speech_generated[:,320:480], wb_speech[:,320:480].detach()))
            total_loss = tf_loss +  0.03*cos_dist_loss

            total_loss.backward()
            
            g_optimizer.step()

            if (i + 1) % 1 == 0:
                print(f'Epoch {epoch + 1}, Step {i + 1}, spect_loss {tf_loss.item()}, cos_dist {cos_dist_loss.item()}\n') 
              
            if ((epoch % 1 == 0) and (i % 100 == 0)):

                model_path = os.path.join(models_path, 'generator.pkl')
                torch.save(generator.state_dict(), model_path)

                model_opt_path = os.path.join(optimizer_path, 'g_opt.pkl')
                torch.save(g_optimizer.state_dict(), model_opt_path) 

    print('Finished Training!')

