"""STFT-based Loss modules."""

import torch
import torch.nn.functional as F
import numpy as np

Fs = 16000

def gen_filterbank(N, device):
    in_freq = (np.arange(N+1, dtype='float32')/N*Fs/2)[None,:]
    out_freq = (np.arange(N, dtype='float32')/N*Fs/2)[:,None]
    #ERB from B.C.J Moore, An Introduction to the Psychology of Hearing, 5th Ed., page 73.
    ERB_N = 24.7 + .108*in_freq
    delta = np.abs(in_freq-out_freq)/ERB_N
    center = (delta<.5).astype('float32')
    R = -12*center*delta**2 + (1-center)*(3-12*delta)
    RE = 10.**(R/10.)
    norm = np.sum(RE, axis=0)
    #print(norm.shape)
    RE = RE/norm
    return torch.tensor(RE, device=device)

def gen_weight(N, device):
    freq = np.arange(N, dtype='float32')/N*Fs/2
    #ERB from B.C.J Moore, An Introduction to the Psychology of Hearing, 5th Ed., page 73.
    ERB_N = 24.7 + .108*freq
    W = 240./ERB_N
    W = W[None,:,None]
    return torch.tensor(np.sqrt(W), device=device)

'''def spec_loss(y_true, y_pred, mask, alpha, fweight):
    diff = (y_true+1e-6)**alpha - (y_pred+1e-6)**alpha
    den = (1e-6+mask)**alpha
    diff = diff[:,:-1,:] / den
    return torch.mean(fweight*torch.square(diff))/(alpha**2)

def lsd_loss(y_true, y_pred, fweight):
    T = 10*torch.log10(1e-8+y_true)
    P = 10*torch.log10(1e-8+y_pred)
    return torch.mean(fweight*torch.square(T-P))'''

def stft_jm(x, fft_size, hop_size, win_length, window):
    """Perform STFT and convert to magnitude spectrogram & apply a filterbank.
    Args:
        x (Tensor): Input signal tensor (B, T).
        fft_size (int): FFT size.
        hop_size (int): Hop size.
        win_length (int): Window length.
        window (str): Window function type.
    Returns:
        Tensor: Magnitude spectrogram (B, #frames, fft_size // 2 + 1).
    """

    X = torch.stft(x, fft_size, hop_length=hop_size, win_length=win_length, return_complex=True, center=False, window=window)
    X = torch.abs(X)**2
    
    return X

def stft(x, fft_size, hop_size, win_length, window):
    """Perform STFT and convert to magnitude spectrogram.
    Args:
        x (Tensor): Input signal tensor (B, T).
        fft_size (int): FFT size.
        hop_size (int): Hop size.
        win_length (int): Window length.
        window (str): Window function type.
    Returns:
        Tensor: Magnitude spectrogram (B, #frames, fft_size // 2 + 1).
    """
    
    #x_stft = torch.stft(x, fft_size, hop_size, win_length, window, return_complex=False)
    #real = x_stft[..., 0]
    #imag = x_stft[..., 1]

    # (kan-bayashi): clamp is needed to avoid nan or inf
    #return torchaudio.functional.amplitude_to_DB(torch.abs(x_stft),db_multiplier=0.0, multiplier=20,amin=1e-05,top_db=80)
    #return torch.clamp(torch.abs(x_stft), min=1e-7)

    x_stft = torch.stft(x, fft_size, hop_size, win_length, window, return_complex=True)
    return torch.clamp(torch.abs(x_stft), min=1e-7), torch.real(x_stft)

class SpectralConvergenceLoss(torch.nn.Module):
    """Spectral convergence loss module."""

    def __init__(self):
        """Initilize spectral convergence loss module."""
        super(SpectralConvergenceLoss, self).__init__()

    def forward(self, x_mag, y_mag):
        """Calculate forward propagation.
        Args:
            x_mag (Tensor): Magnitude spectrogram of predicted signal (B, #frames, #freq_bins).
            y_mag (Tensor): Magnitude spectrogram of groundtruth signal (B, #frames, #freq_bins).
        Returns:
            Tensor: Spectral convergence loss value.
        """
        #dims=list(range(1, len(x_mag.shape)))
        #return torch.mean(torch.norm(torch.abs(y_mag) - torch.abs(x_mag), p="fro", dim=dims) / (torch.norm(x_mag, p="fro", dim=dims) + 1e-6))

        return torch.norm(y_mag - x_mag, p="fro") / (torch.norm(y_mag, p="fro"))#torch.norm(y_mag - x_mag, p="fro") / (torch.norm(x_mag, p="fro") + 1e-6) #torch.norm(y_mag - x_mag, p="fro") / torch.norm(y_mag, p="fro")#

class RealSTFTLoss(torch.nn.Module):

    def __init__(self):

        super(RealSTFTLoss, self).__init__()

    def forward(self, x_real, y_real):
        weight = torch.tensor(np.linspace(1, 5, x_real.size(1))).to(x_real.device).view(1,x_real.size(1),1)
        x_real = x_real * weight
        y_real = y_real * weight

        return F.l1_loss(x_real, y_real) #torch.norm(y_mag - x_mag, p="fro") / (torch.norm(y_mag, p="fro"))
    
class LogSTFTMagnitudeLoss(torch.nn.Module):
    """Log STFT magnitude loss module."""

    def __init__(self):
        """Initilize los STFT magnitude loss module."""
        super(LogSTFTMagnitudeLoss, self).__init__()

    def forward(self, x, y):
        """Calculate forward propagation.
        Args:
            x_mag (Tensor): Magnitude spectrogram of predicted signal (B, #frames, #freq_bins).
            y_mag (Tensor): Magnitude spectrogram of groundtruth signal (B, #frames, #freq_bins).
        Returns:
            Tensor: Log STFT magnitude loss value.
        """

        #y_mag = torch.sqrt(1e-7 + y)
        #x_mag = torch.sqrt(1e-7 + x)

        error_loss = F.l1_loss(x, y) + F.l1_loss(x[:,:,1:] - x[:,:,:-1], y[:,:,1:] - y[:,:,:-1]) #  F.l1_loss(torch.log(x + 1e-15), torch.log(y + 1e-15)) #
        
        return error_loss  #torch.norm(y_mag - x_mag, p=1) / torch.norm(y_mag, p=1) #
        
class JMLoss(torch.nn.Module):
    """Spectral convergence loss module."""

    def __init__(self):
        """Initilize spectral convergence loss module."""
        super(JMLoss, self).__init__()

    def lsd_loss(self, y_true, y_pred, fweight):
        T = 10*torch.log10(1e-8+y_true)
        P = 10*torch.log10(1e-8+y_pred)
        return torch.mean(fweight*torch.square(T-P))

    def spec_loss(self, y_true, y_pred, mask, alpha, fweight):
        diff = (y_true+1e-6)**alpha - (y_pred+1e-6)**alpha
        den = (1e-6+mask)**alpha
        diff = diff[:,:-1,:] / den
        return torch.mean(fweight*torch.square(diff))/(alpha**2)

    def forward(self, y_true, y_pred, mask_true, mask_pred, alpha, fweight):

        spec_loss = self.spec_loss(y_true, y_pred, mask_true, alpha, fweight)
        lsd_loss = self.lsd_loss(mask_true, mask_pred, fweight)

        return spec_loss + .05*lsd_loss
    
class STFTLoss(torch.nn.Module):
    """STFT loss module."""

    def __init__(self, device, fft_size=1024, shift_size=120, win_length=600, window="hann_window"):
        """Initialize STFT loss module."""
        super(STFTLoss, self).__init__()
        self.fft_size = fft_size
        self.shift_size = shift_size
        self.win_length = win_length
        self.window = getattr(torch, window)(win_length).to(device)
        self.spectral_convergenge_loss = SpectralConvergenceLoss()
        self.log_stft_magnitude_loss = LogSTFTMagnitudeLoss()
        self.real_stft_loss = RealSTFTLoss()
        self.alpha = 0.3
        self.jm_loss = JMLoss()

    def forward(self, x, y):
        """Calculate forward propagation.
        Args:
            x (Tensor): Predicted signal (B, T).
            y (Tensor): Groundtruth signal (B, T).
        Returns:
            Tensor: Spectral convergence loss value.
            Tensor: Log STFT magnitude loss value.
        """
        x_mag, x_real = stft(x, self.fft_size, self.shift_size, self.win_length, self.window)
        y_mag, y_real = stft(y, self.fft_size, self.shift_size, self.win_length, self.window)

        sc_loss = self.spectral_convergenge_loss(x_mag, y_mag)
        mag_loss = self.log_stft_magnitude_loss(x_mag, y_mag)
        real_loss = self.real_stft_loss(x_real, y_real)

        '''N = self.fft_size

        ind = np.arange(N, dtype='float32')
        wind = np.sin(.5*np.pi*np.sin((ind+.5)/N*np.pi)**2)
        wind = torch.tensor(wind, device=x.device)
        mask = gen_filterbank(N//2, x.device)
        weight = gen_weight(N//2, x.device)

        x_mag_squared  = stft_jm(x, self.fft_size, self.shift_size, self.win_length, wind)
        y_mag_squared = stft_jm(y, self.fft_size, self.shift_size, self.win_length, wind)

        x_mask = torch.matmul(mask, x_mag_squared)
        y_mask = torch.matmul(mask, y_mag_squared)
        
        lsd_pls_spectral_loss = self.jm_loss(y_mag_squared, x_mag_squared, y_mask, x_mask, self.alpha, weight)'''

        return sc_loss, mag_loss, real_loss#, lsd_pls_spectral_loss

class MultiResolutionSTFTLoss(torch.nn.Module):
        
    def __init__(self,
        device,
        fft_sizes= [2560, 1280, 640, 320, 160, 80],
        hop_sizes= [640, 320, 160, 80, 40, 20],
        win_lengths= [2560, 1280, 640, 320, 160, 80],
        window="bartlett_window"):

        '''def __init__(self,
                            device,
                            fft_sizes=[2048, 1024, 512, 256, 128, 64],
                            hop_sizes=[512, 256, 128, 64, 32, 16],
                            win_lengths=[2048, 1024, 512, 256, 128, 64],
                            window="bartlett_window"):'''

        super(MultiResolutionSTFTLoss, self).__init__()
        assert len(fft_sizes) == len(hop_sizes) == len(win_lengths)
        self.stft_losses = torch.nn.ModuleList()
        for fs, ss, wl in zip(fft_sizes, hop_sizes, win_lengths):
            self.stft_losses += [STFTLoss(device, fs, ss, wl, window)]

    def forward(self, x, y):
        """Calculate forward propagation.
        Args:
            x (Tensor): Predicted signal (B, T).
            y (Tensor): Groundtruth signal (B, T).
        Returns:
            Tensor: Multi resolution spectral convergence loss value.
            Tensor: Multi resolution log STFT magnitude loss value.
        """
        sc_loss = 0.0
        mag_loss = 0.0
        lsd_spect_loss  = 0.0
        real_loss = 0.0

        for f in self.stft_losses:
            sc_l, mag_l, real_l = f(x, y)#sc_l, mag_l, lsd_spect_l = f(x, y)
            #sc_loss += sc_l
            #real_loss += real_l
            #lsd_spect_loss += lsd_spect_l
            mag_loss += mag_l
        sc_loss /= len(self.stft_losses)
        mag_loss /= len(self.stft_losses)
        lsd_spect_loss /= len(self.stft_losses)
        real_loss /= len(self.stft_losses)

        return mag_loss #+ sc_loss # real_loss  + lsd_spect_loss#