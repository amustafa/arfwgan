import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
import numpy as np

which_norm = weight_norm
kernel_size = 3

class CausalConv(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(CausalConv, self).__init__()
        torch.manual_seed(5)
       
        self.padding = (kernel_size - 1) * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding , 0 ))
        conv_out = self.nl(self.conv(x))
        return conv_out

class ConvLookahead(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size, dilation=1, groups=1, bias= False):
        super(ConvLookahead, self).__init__()
        torch.manual_seed(5)

        self.padding_left = (kernel_size - 2) * dilation
        self.padding_right = 1 * dilation
        
        self.conv = which_norm(nn.Conv1d(in_ch,out_ch,kernel_size,dilation=dilation, groups=groups, bias= bias))
        self.nl = nn.Tanh()

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):

        x = F.pad(x,(self.padding_left, self.padding_right))
        
        conv_out = self.nl(self.conv(x))
        return conv_out
    
class GLU(nn.Module):
    def __init__(self, feat_size):
        super(GLU, self).__init__()
        
        torch.manual_seed(5)

        self.gate = which_norm(nn.Linear(feat_size, feat_size, bias=False))

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d)\
            or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
        
        out = x * torch.sigmoid(self.gate(x)) 
    
        return out
    
class ForwardGRU(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1):
        super(ForwardGRU, self).__init__()
        
        torch.manual_seed(5)
        
        self.hidden_size = hidden_size

        self.gru = nn.GRU(input_size=input_size, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,\
                          bias=False)

        self.nl = GLU(self.hidden_size)
        
        self.h0 = None

        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def one_forward_pass(self, one_frame):
        
        if self.h0 is None:

            output, self.h0 = self.gru(one_frame)

        else:

            output, self.h0 = self.gru(one_frame, self.h0)

        return self.nl(output) 
    
    def forward(self, x_frame):
    
        return self.one_forward_pass(x_frame) 

    def reset(self):
        self.h0 = None
        return
    
class FramewiseConv(torch.nn.Module):
    
    def __init__(self, frame_len, out_dim, frame_kernel_size=3, act='glu', causal=True):
        
        super(FramewiseConv, self).__init__()
        torch.manual_seed(5)
        
        self.frame_kernel_size = frame_kernel_size
        self.frame_len = frame_len

        if (causal == True) or (self.frame_kernel_size == 2):

            self.required_pad_left = (self.frame_kernel_size - 1) * self.frame_len
            self.required_pad_right = 0 

        else:

            self.required_pad_left = (self.frame_kernel_size - 1)//2 * self.frame_len
            self.required_pad_right = (self.frame_kernel_size - 1)//2 * self.frame_len
        
        self.fc_input_dim = self.frame_kernel_size * self.frame_len
        self.fc_out_dim = out_dim
        
        if act=='glu':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh(),
                                    GLU(self.fc_out_dim) 
                                   )
        if act=='tanh':
            self.fc = nn.Sequential(which_norm(nn.Linear(self.fc_input_dim, self.fc_out_dim, bias=False)),
                                    nn.Tanh()
                                   )
        
        self.buffer = None
        
        self.init_weights()
                
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)
                
    def one_forward_pass(self, one_frame):
        
        if self.buffer is None:
            self.buffer = torch.zeros(one_frame.size(0),1,self.required_pad_left,device=one_frame.device)
            
        one_frame_flat = one_frame.reshape(one_frame.size(0),1,-1)
        one_frame_flat_padded = torch.cat((self.buffer, one_frame_flat), dim=-1)
        out_frame_new = self.fc(one_frame_flat_padded)
        self.buffer[:,:,:self.frame_len] = self.buffer[:,:,self.frame_len:]
        self.buffer[:,:,self.frame_len:] = one_frame_flat
        return out_frame_new

    def forward(self, x_frame):
        
        return self.one_forward_pass(x_frame)
    
    def reset(self):
        self.buffer = None
        return
    
class UpsampleFC(nn.Module):
    def __init__(self, in_ch, out_ch, upsample_factor):
        super(UpsampleFC, self).__init__()
        torch.manual_seed(5)
        
        self.in_ch = in_ch
        self.out_ch = out_ch
        self.upsample_factor = upsample_factor
        self.fc = nn.Linear(in_ch, out_ch * upsample_factor, bias=False)
        self.nl = nn.Tanh()
        
        self.init_weights()

    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or\
            isinstance(m, nn.Linear) or isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def forward(self, x):
                
        batch_size = x.size(0)
        x = x.permute(0, 2, 1)
        x = self.nl(self.fc(x))
        x = x.reshape((batch_size, -1, self.out_ch))
        x = x.permute(0, 2, 1)
        return x
        
class PSARFWGAN(nn.Module):
    def __init__(self):
        super().__init__()
        torch.manual_seed(5)

        self.pembed = nn.Embedding(257-32, 32)

        self.bfcc_with_corr_dense = nn.Sequential(which_norm(nn.Linear(19+32, 80, bias=False)),
                                                  nn.Tanh())

        self.feat_conv1 = ConvLookahead(80,128,3)
        self.glu1 = GLU(128)

        self.feat_conv2 = CausalConv(128,128,3)
        self.glu2 = GLU(128)

        self.feat_upscale = UpsampleFC(128,80,5)
        self.glu3 = GLU(80)

        self.rnn = ForwardGRU(80+64,256)
        
        self.fwc1 = FramewiseConv(256 + 64, 256)
        self.fwc2 = FramewiseConv(256 + 64, 128)
        self.fwc3 = FramewiseConv(128 + 64, 128)
        self.fwc4 = FramewiseConv(128 + 64, 64)
        self.fwc5 = FramewiseConv(64 + 64, 64)
        self.fwc6 = FramewiseConv(64 + 64, 32)
        self.fwc7 = FramewiseConv(32 + 64, 32)
        
        self.init_weights()
        self.count_parameters()
        
    def init_weights(self):
    
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Linear) or\
            isinstance(m, nn.Embedding):
                nn.init.orthogonal_(m.weight.data)

    def count_parameters(self):
        num_params =  sum(p.numel() for p in self.parameters() if p.requires_grad)
        print(f"Total number of {self.__class__.__name__} network parameters = {num_params}\n")
        
    def create_phase_signals(self, periods):

        batch_size = periods.size(0)
        progression = torch.arange(1, 160 + 1, dtype=periods.dtype, device=periods.device).view((1, -1))
        progression = torch.repeat_interleave(progression, batch_size, 0)

        phase0 = torch.zeros(batch_size, dtype=periods.dtype, device=periods.device).unsqueeze(-1)
        chunks = []
        for sframe in range(periods.size(1)):
            f = (2.0 * torch.pi / periods[:, sframe]).unsqueeze(-1)
            
            chunk_sin = torch.sin(f  * progression + phase0)
            chunk_sin = chunk_sin.reshape(chunk_sin.size(0),-1,32)
            
            chunk_cos = torch.cos(f  * progression + phase0)
            chunk_cos = chunk_cos.reshape(chunk_cos.size(0),-1,32)
            
            chunk = torch.cat((chunk_sin, chunk_cos), dim = -1)
            
            phase0 = phase0 + 160 * f

            chunks.append(chunk)
            
        phase_signals = torch.cat(chunks, dim=1)
        
        return phase_signals
    
    def forward(self, pitch_period, bfcc_with_corr, x0):

        c0 = torch.repeat_interleave(bfcc_with_corr[:,:,:1], 5, 1)
        gain = .03*10**(0.5*c0/np.sqrt(18.0))
        
        p_embed = self.pembed(pitch_period-32)
                
        feat = torch.cat((p_embed , bfcc_with_corr), dim=-1)
        
        feat_dense = self.bfcc_with_corr_dense(feat).permute(0,2,1).contiguous()
        
        feat_act1 = self.glu1(self.feat_conv1(feat_dense).permute(0,2,1).contiguous()).permute(0,2,1).contiguous()
        
        feat_act2 = self.glu2(self.feat_conv2(feat_act1).permute(0,2,1).contiguous()).permute(0,2,1).contiguous()

        feat_latent = self.glu3(self.feat_upscale(feat_act2).permute(0,2,1).contiguous())
        
        period_repeat = torch.repeat_interleave(pitch_period, 5, dim=-1)
        batch_size = x0.size(0)
        device = x0.device
        # Pre-load the first 10 ms of x0 in the memory
        pitch_mem = torch.cat([torch.zeros((batch_size,256-160), device=device), x0[:,:160]], dim=-1)
        ar_wav = x0[:, 128:160].unsqueeze(1)
        out_waveform = x0

        # Start running the model on the last 10 ms of x0
        for i in range(5, feat_latent.size(1)):

            T = period_repeat[:, i:i+1]
            ind = 256 - T
            ind = ind + torch.arange(32, device=ind.device)
            pred_wav = torch.gather(pitch_mem, dim=-1, index=ind).unsqueeze(1) 

            pred_wav = torch.clamp(pred_wav/(1e-5 + gain[:,i:i+1,:]), min=-1, max=1)
            
            ar_wav = torch.clamp(ar_wav/(1e-5 + gain[:,i:i+1,:]), min=-1, max=1)
            
                        
            latent_frame = torch.cat((feat_latent[:,i:i+1,:], pred_wav, ar_wav), dim=-1) 
            
            rnn_out = self.rnn(latent_frame)

            fwc1_out = self.fwc1(torch.cat((rnn_out, pred_wav, ar_wav), dim=-1))
            fwc2_out = self.fwc2(torch.cat((fwc1_out, pred_wav, ar_wav), dim=-1))
            fwc3_out = self.fwc3(torch.cat((fwc2_out, pred_wav, ar_wav), dim=-1))
            fwc4_out = self.fwc4(torch.cat((fwc3_out, pred_wav, ar_wav), dim=-1))
            fwc5_out = self.fwc5(torch.cat((fwc4_out, pred_wav, ar_wav), dim=-1))    
            fwc6_out = self.fwc6(torch.cat((fwc5_out, pred_wav, ar_wav), dim=-1))
            fwc7_out = self.fwc7(torch.cat((fwc6_out, pred_wav, ar_wav), dim=-1))

            if i<10:

                out_frame = x0[:,i*32:(i+1)*32]

            else:

                out_frame =  fwc7_out.squeeze(1) * gain[:,i:i+1,:].squeeze(1)
                out_waveform = torch.cat((out_waveform, out_frame), dim=-1)

            ar_wav = out_frame.unsqueeze(1)
            pitch_mem = torch.cat([pitch_mem[:,32:], out_frame], dim=-1)

        self.rnn.reset()
        self.fwc1.reset()
        self.fwc2.reset()
        self.fwc3.reset()
        self.fwc4.reset()
        self.fwc5.reset()
        self.fwc6.reset()
        self.fwc7.reset()

        return  out_waveform 